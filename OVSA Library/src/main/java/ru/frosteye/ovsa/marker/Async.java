package ru.frosteye.ovsa.marker;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by oleg on 14.12.16.
 */
@Retention(RetentionPolicy.SOURCE)
public @interface Async {
}
