package ru.frosteye.ovsa.data.storage;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ColorRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;

/**
 * Created by ovcst on 07.01.2017.
 */

public class ResourceHelper {

    private static Resources resources;
    private static Context appContext;

    public ResourceHelper(Context context) {
        appContext = context.getApplicationContext();
        resources = context.getApplicationContext().getResources();
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static String getString(@StringRes int res) {
        return resources.getString(res);
    }

    public static String getString(@StringRes int res, Object... args) {
        return resources.getString(res, args);
    }

    public static Resources getResources() {
        return resources;
    }

    public static int getInteger(@IntegerRes int res) {
        return resources.getInteger(res);
    }

    public static int getColor(@ColorRes int res) {
        return resources.getColor(res);
    }
}
