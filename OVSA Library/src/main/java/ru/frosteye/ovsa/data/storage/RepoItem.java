package ru.frosteye.ovsa.data.storage;

/**
 * Created by ovcst on 08.06.2017.
 */

public interface RepoItem {
    String getKey();
}
