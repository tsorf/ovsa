package ru.frosteye.ovsa.data.constant;

/**
 * Created by oleg on 20.12.16.
 */

public class OvsaKeys {
    public static final String META = "_meta";
    public static final String LINKS = "_links";
    public static final String AUTHORIZATION = "Authorization";
}
