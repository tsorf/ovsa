package ru.frosteye.ovsa.data.entity;

import android.location.Location;

import java.io.Serializable;

import ru.frosteye.ovsa.execution.serialization.Serializer;

/**
 * Created by oleg on 20.12.16.
 */

public class SimpleLocation implements Serializable {
    public static final String KEY = "simple_location";
    private double lat;
    private double lng;

    public SimpleLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public SimpleLocation(Location location) {
        this.lat = location.getLatitude();
        this.lng = location.getLongitude();
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
