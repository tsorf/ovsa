package ru.frosteye.ovsa.security.permissions;

import android.app.Activity;

//import com.tbruyelle.rxpermissions.RxPermissions;

import retrofit2.Call;
import rx.functions.Action1;

/**
 * Created by oleg on 29.12.16.
 */

public class PermissionsHelper {

    private Activity activity;

    public PermissionsHelper(Activity activity) {
        this.activity = activity;
    }

    public boolean isGranted(String permission) {
        //return new RxPermissions(activity).isGranted(permission);
        return true;
    }

    public void requestPermissions(Callback callback, String... permissions) {
        requestPermissions(null, callback, permissions);
    }

    public void requestPermissions(String message, final Callback callback, String... permissions) {
        callback.onResult(true);
        /*new RxPermissions(activity)
                .request(permissions)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        callback.onResult(aBoolean);
                    }
                });*/
    }

    public interface Callback {
        void onResult(boolean granted);
    }
}
