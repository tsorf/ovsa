package ru.frosteye.ovsa.stub.impl;

import android.support.design.widget.TabLayout;

/**
 * Created by oleg on 09.09.16.
 */
public class SimpleTabSelectListener implements TabLayout.OnTabSelectedListener {
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
