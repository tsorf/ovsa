package ru.frosteye.ovsa.stub.view;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

/**
 * Created by oleg on 04.07.16.
 */
public class RefreshableSwipeRefreshLayout extends SwipeRefreshLayout {
    public RefreshableSwipeRefreshLayout(Context context) {
        super(context);
    }

    public RefreshableSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        if(!refreshing) {
            super.setRefreshing(false);
        } else {
            post(new Runnable() {
                @Override
                public void run() {
                    RefreshableSwipeRefreshLayout.super.setRefreshing(true);
                }
            });
        }
    }
}
