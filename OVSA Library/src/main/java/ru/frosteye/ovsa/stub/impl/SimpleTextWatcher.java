package ru.frosteye.ovsa.stub.impl;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by oleg on 21.06.16.
 */
public class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
