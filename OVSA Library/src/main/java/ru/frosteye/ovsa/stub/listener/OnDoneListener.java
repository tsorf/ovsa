package ru.frosteye.ovsa.stub.listener;

/**
 * Created by oleg on 01.07.16.
 */
public interface OnDoneListener {
    void onDone();
}
