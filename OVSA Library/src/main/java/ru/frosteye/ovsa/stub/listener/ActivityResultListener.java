package ru.frosteye.ovsa.stub.listener;

import android.content.Intent;

/**
 * Created by user on 19.07.16.
 */
public interface ActivityResultListener {
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
