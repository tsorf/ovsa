package ru.frosteye.ovsa.presentation.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Handler;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorRes;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import ru.frosteye.ovsa.R;
import ru.frosteye.ovsa.execution.serialization.Serializer;
import ru.frosteye.ovsa.presentation.callback.SimpleTextChangeCallback;
import ru.frosteye.ovsa.security.Verificator;
import ru.frosteye.ovsa.security.permissions.PermissionsHelper;
import ru.frosteye.ovsa.stub.impl.SimpleTextWatcher;
import ru.frosteye.ovsa.stub.listener.OnDoneListener;
import ru.frosteye.ovsa.stub.listener.SelectListener;
import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.ovsa.tool.UITools;

/**
 * Created by oleg on 21.06.16.
 */
public abstract class OvsaActivity extends AppCompatActivity {
    public static final String TAG = OvsaActivity.class.getSimpleName();
    private static final int MENU_LOADING = 123;
    private Map<SimpleTextWatcher, SimpleTextChangeCallback> textChangeListeners = new HashMap<>();
    private boolean backButtonEnabled = false;
    private boolean topLoadingShown = false;
    private Verificator verificator;
    private PermissionsHelper helper = new PermissionsHelper(this);

    protected void registerTextChangeListeners(SimpleTextChangeCallback changeCallback, TextView... views) {
        SimpleTextWatcher textWatcher = new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                textChangeListeners.get(this).textChanged(editable);
            }
        };
        for(TextView view: views) {
            view.addTextChangedListener(textWatcher);
        }
        textChangeListeners.put(textWatcher, changeCallback);
    }

    protected void registerTextChangeListeners(final View viewToEnable, final TextView... views) {
        SimpleTextWatcher textWatcher = new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {

                viewToEnable.setEnabled(checkEnabledConditions(viewToEnable, views));
            }
        };
        for(TextView view: views) {
            view.addTextChangedListener(textWatcher);
        }
    }

    protected void applyBackArrowColor(@ColorRes int color) {
        try {
            Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(ContextCompat.getColor(this, color), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

        } catch (Exception ignored) {
        }
    }

    protected void startActivityAndClearTask(Class<? extends Activity> clazz) {
        Intent intents = new Intent(this, clazz);
        startActivityAndClearTask(intents);
    }

    protected void startActivityAndClearTask(Intent intents) {
        intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intents);
        finish();
    }

    protected boolean checkEnabledConditions(View viewToEnable, TextView... views) {
        for(TextView view: views) {
            if(TextTools.isTrimmedEmpty(view)) {
                return false;
            }
        }
        return true;
    }

    public void setEditTextColorFilter(EditText input, int color) {
        input.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    public boolean isPermissionGranted(String permission) {
        return helper.isGranted(permission);
    }

    public void requestPermissions(PermissionsHelper.Callback callback, String... permissions) {
        requestPermissions(null, callback, permissions);
    }

    public void requestPermissions(String message, final PermissionsHelper.Callback callback,
                                   String... permissions) {
        helper.requestPermissions(message, callback, permissions);
    }

    protected void enableBackButton() {
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            backButtonEnabled = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected <T> T getSerializable(String key, Class<T> clazz) {
        if(getIntent().getExtras() == null) return null;
        Serializable serializable = getIntent().getSerializableExtra(key);
        if(serializable != null && serializable.getClass() == clazz) {
            return clazz.cast(serializable);
        } else return null;
    }

    protected void setOnDoneListener(TextView textView, final OnDoneListener onDoneListener) {
        textView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    onDoneListener.onDone();
                }
                return false;
            }
        });
    }

    public void showTopBarLoading(boolean shown) {
        this.topLoadingShown = shown;
        invalidateOptionsMenu();
    }

    public AnimatedVectorDrawableCompat getLoaderDrawable() {
        try {
            AnimatedVectorDrawableCompat icon = AnimatedVectorDrawableCompat.create(this, R.drawable.ovsa_menu_loader_animated);

            return icon;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            Drawable icon = AnimatedVectorDrawableCompat.create(this, R.drawable.ovsa_menu_loader_animated);
            menu.add(0, MENU_LOADING, 0, "Loading")
                    .setIcon(icon)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        } catch (Exception ignored) {
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void startActivity(Class<? extends Activity> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

    public void startActivity(Class<? extends Activity> clazz, boolean finish) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
        if(finish) finish();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        processTopLoading(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    private void processTopLoading(Menu menu) {
        try {
            MenuItem menuItemLoader = menu.findItem(MENU_LOADING);
            Drawable menuItemLoaderIcon = menuItemLoader.getIcon();
            if (menuItemLoaderIcon != null) {
                if(topLoadingShown) {
                    menuItemLoader.setVisible(true);
                    ((Animatable) menuItemLoader.getIcon()).start();
                } else {
                    menuItemLoader.setVisible(false);
                }
            }
        } catch (Exception ignored) {

        }
    }

    protected void showKeyboard(final EditText editText) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 500);
    }



    @Deprecated
    public void showMessage(String message) {
        UITools.toastLong(this, message);
    }

    @Deprecated
    public void showMessage(CharSequence message, int type) {
        if(message == null) return;
        UITools.toastLong(this, message.toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)     {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(!backButtonEnabled) break;
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showSelect(Context context,
                              @ArrayRes int res,
                              final SelectListener listener) {
        showSelect(context, res, listener, false);
    }

    protected void showSelect(Context context,
                              @ArrayRes int res,
                              final SelectListener listener, boolean notCancelable) {
        final String[] titles = context.getResources().getStringArray(res);
        showSelect(context, titles, listener, notCancelable);
    }

    protected void showSelect(Context context,
                              final String[] items,
                              final SelectListener listener, boolean notCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onSelect(items[i], i);
            }
        });
        builder.setCancelable(!notCancelable);
        builder.show();
    }

    public void setVerificator(Verificator verificator) {
        this.verificator = verificator;
    }

    public void verifyApp(Verificator.Callback callback) {
        if(verificator == null) return;
        verificator.performCheck(callback);
    }
}
