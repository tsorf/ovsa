package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import ru.frosteye.ovsa.R;

/**
 * Created by oleg on 21.06.16.
 */
public class IconizedEditText extends LinearLayout {
    private AppCompatEditText inputView;
    private AppCompatImageView iconView;

    private String hint;
    private Drawable icon;

    public IconizedEditText(Context context) {
        super(context);
    }

    public IconizedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.IconizedEditText));

    }

    public IconizedEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.IconizedEditText, defStyleAttr, 0));
    }

    private void init(TypedArray array) {
        if(array.hasValue(R.styleable.IconizedEditText_hint)) {
            this.hint = array.getString(R.styleable.IconizedEditText_hint);
        }
        if(array.hasValue(R.styleable.IconizedEditText_iconDrawable)) {
            this.icon = array.getDrawable(R.styleable.IconizedEditText_iconDrawable);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inflate(getContext(), R.layout.ovsa_iconized_edit_text, this);
        this.inputView = ((AppCompatEditText) findViewById(R.id.view_iconizedEditText_input));
        this.iconView = ((AppCompatImageView) findViewById(R.id.view_iconizedEditText_icon));
        this.iconView.setImageDrawable(icon);
        this.inputView.setHint(hint);
    }

    public AppCompatEditText getInput() {
        return inputView;
    }

    public void setInputType(int inputType) {
        this.inputView.setInputType(inputType);
    }

    public String getText() {
        return inputView.getText().toString();
    }

    public void setHint(String hint) {
        this.inputView.setHint(hint);
    }

    public void setHint(@StringRes int hint) {
        this.inputView.setHint(hint);
    }

    public void setIcon(Drawable icon) {
        this.iconView.setImageDrawable(icon);
    }

    public void setIcon(@DrawableRes int icon) {
        this.iconView.setImageResource(icon);
    }
}