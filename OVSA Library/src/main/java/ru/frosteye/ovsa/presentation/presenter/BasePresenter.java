package ru.frosteye.ovsa.presentation.presenter;

import android.support.annotation.CallSuper;

import ru.frosteye.ovsa.presentation.view.BasicView;

/**
 * Created by ovcst on 08.06.2017.
 */

public abstract class BasePresenter<V extends BasicView> implements LivePresenter<V> {

    protected V view;


    @Override @CallSuper
    public void onAttach(V v) {
        this.view = v;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public abstract void onDestroy();

    protected void showMessage(CharSequence message) {
        view.showMessage(message, 0);
    }

    protected void enableControls(boolean enabled) {
        view.enableControls(enabled, 0);
    }
}
