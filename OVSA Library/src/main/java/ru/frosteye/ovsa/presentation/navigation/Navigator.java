package ru.frosteye.ovsa.presentation.navigation;

import android.view.MenuItem;

/**
 * Created by oleg on 10.07.17.
 */

public interface Navigator<V extends NavigatorView> {

    void onNavigatorAction(Action action);
    void onAttachView(V view);
    boolean onOptionsItemSelected(MenuItem item);

    interface Action {
        int code();
    }
}

