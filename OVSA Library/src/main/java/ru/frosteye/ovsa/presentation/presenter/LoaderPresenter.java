package ru.frosteye.ovsa.presentation.presenter;

import java.util.List;

/**
 * Created by oleg on 01.07.16.
 */
public interface LoaderPresenter<T, V> extends LivePresenter<V> {
    void load();
    List<T> getCached();
}
