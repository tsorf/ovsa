package ru.frosteye.ovsa.presentation.navigation.impl;

import ru.frosteye.ovsa.presentation.navigation.Navigator;

/**
 * Created by ovcst on 05.07.2017.
 */

public class SimpleNavAction implements Navigator.Action {

    private int code;

    public SimpleNavAction(int code) {
        this.code = code;
    }

    @Override
    public int code() {
        return code;
    }
}
