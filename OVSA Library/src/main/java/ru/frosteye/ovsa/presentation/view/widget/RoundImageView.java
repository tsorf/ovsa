package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedImageView;

import ru.frosteye.ovsa.tool.picture.ImageTarget;

/**
 * Created by user on 02.07.16.
 */
public class RoundImageView extends RoundedImageView implements ImageTarget {
    public RoundImageView(Context context) {
        super(context);
    }

    public RoundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    public ImageView getView() {
        return this;
    }
}
