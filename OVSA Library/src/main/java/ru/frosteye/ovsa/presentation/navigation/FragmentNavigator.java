package ru.frosteye.ovsa.presentation.navigation;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by oleg on 27.06.16.
 */

@Deprecated
public interface FragmentNavigator<T extends Fragment> {
    void setContainerResource(@IdRes int resource);
    void showFragment(AppCompatActivity activity, T fragment);
    void showFragment(AppCompatActivity activity, T fragment, boolean backstack);
}
