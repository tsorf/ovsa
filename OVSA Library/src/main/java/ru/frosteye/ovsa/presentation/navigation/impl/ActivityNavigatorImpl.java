package ru.frosteye.ovsa.presentation.navigation.impl;

import android.app.Activity;
import android.content.Intent;

import javax.inject.Inject;

import ru.frosteye.ovsa.presentation.navigation.ActivityNavigator;

/**
 * Created by oleg on 28.06.16.
 */
@Deprecated
public class ActivityNavigatorImpl implements ActivityNavigator {
    @Inject
    public ActivityNavigatorImpl() {
    }

    @Override
    public void runActivity(Activity from, Class<?> clazz) {
        from.startActivity(new Intent(from, clazz));
    }

    @Override
    public void runActivity(Activity from, Intent intent) {
        from.startActivity(intent);
    }

    @Override
    public void runActivityForResult(Activity from, int requestCode, Class<?> clazz) {
        from.startActivityForResult(new Intent(from, clazz), requestCode);
    }
}
