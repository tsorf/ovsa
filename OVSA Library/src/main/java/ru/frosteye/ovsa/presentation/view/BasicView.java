package ru.frosteye.ovsa.presentation.view;

/**
 * Created by ovcst on 06.04.2017.
 */

public interface BasicView {
    void enableControls(boolean enabled, int code);
    void showMessage(CharSequence message, int code);

}
