package ru.frosteye.ovsa.presentation.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import ru.frosteye.ovsa.presentation.view.ModelView;


@Deprecated
public class ModelAdapter<M extends ViewModel, V extends View & ModelView<M>>
        extends BaseAdapter<M, ModelAdapter.ModelViewHolder> {

    public ModelAdapter(Context context) {
        super(context);
    }

    public ModelAdapter(Context context, ClickCallback<M> clickCallback) {
        super(context, clickCallback);
    }

    public ModelAdapter(Context context, ClickCallback<M> clickCallback, LongClickCallback<M> longClickCallback) {
        super(context, clickCallback, longClickCallback);
    }

    public ModelAdapter(Context context, LongClickCallback<M> longClickCallback) {
        super(context, longClickCallback);
    }

    @Override
    @SuppressWarnings("unchecked")
    public ModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ModelViewHolder((V)inflater.inflate(viewType, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getLayoutId();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(ModelAdapter.ModelViewHolder holder, int position) {
        ((ModelViewHolder)holder).view.setModel(getItem(position));
    }

    public class ModelViewHolder extends BaseAdapter.ViewHolder {

        public final V view;

        public ModelViewHolder(V itemView) {
            super(itemView);
            this.view = itemView;
        }
    }
}
