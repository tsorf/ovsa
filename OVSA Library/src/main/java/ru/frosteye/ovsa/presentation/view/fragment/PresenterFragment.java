package ru.frosteye.ovsa.presentation.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

/**
 * Created by oleg on 15.06.16.
 */
public abstract class PresenterFragment extends OvsaFragment  {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        prepareView(view);
        initView(view);
        attachPresenter();
    }

    protected void prepareView(View view) {}
    protected abstract void initView(View view);
    protected abstract void attachPresenter();
    protected abstract LivePresenter<?> getPresenter();

    @Override
    public void onPause() {
        super.onPause();
        if(getPresenter() != null)
            getPresenter().onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getPresenter() != null)
            getPresenter().onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getPresenter() != null)
            getPresenter().onDestroy();
    }
}
