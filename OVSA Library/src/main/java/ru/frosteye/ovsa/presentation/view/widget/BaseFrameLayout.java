package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleableRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;

/**
 * Created by oleg on 23.12.16.
 */

public abstract class BaseFrameLayout extends FrameLayout {
    public BaseFrameLayout(Context context) {
        super(context);
    }

    public BaseFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource()));
    }

    public BaseFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource(), defStyleAttr, 0));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BaseFrameLayout(Context context, AttributeSet attrs, int defStyleAttr,
                           int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(getStyleableResource() != null)
            init(context.obtainStyledAttributes(attrs, getStyleableResource(), defStyleAttr, defStyleRes));
    }

    @StyleableRes
    protected int[] getStyleableResource() {
        return null;
    }

    @LayoutRes
    protected int getLayoutToInflate() {
        return 0;
    }

    private void init(TypedArray array) {
        onTypedArrayReady(array);
        array.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(getLayoutToInflate() != 0)
            inflate(getContext(), getLayoutToInflate(), this);
        ButterKnife.bind(this);
        prepareView();
    }

    protected void onTypedArrayReady(TypedArray array) { }

    protected abstract void prepareView();
}
