package ru.frosteye.ovsa.presentation.navigation;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by oleg on 27.06.16.
 */

@Deprecated
public interface ActivityNavigator {
    void runActivity(Activity from, Class<?> clazz);
    void runActivity(Activity from, Intent intent);
    void runActivityForResult(Activity from, int requestCode, Class<?> clazz);

}
