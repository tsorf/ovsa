package ru.frosteye.ovsa.presentation.view.fragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.ArrayRes;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import ru.frosteye.ovsa.marker.CrashSafe;
import ru.frosteye.ovsa.presentation.callback.SimpleTextChangeCallback;
import ru.frosteye.ovsa.stub.impl.SimpleTextWatcher;
import ru.frosteye.ovsa.stub.listener.OnDoneListener;
import ru.frosteye.ovsa.stub.listener.SelectListener;
import ru.frosteye.ovsa.tool.UITools;

/**
 * Created by oleg on 27.06.16.
 */
public class OvsaFragment extends Fragment {

    private Map<SimpleTextWatcher, SimpleTextChangeCallback> textChangeListeners = new HashMap<>();


    protected void registerTextChangeListeners(SimpleTextChangeCallback changeCallback, TextView... views) {
        SimpleTextWatcher textWatcher = new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                textChangeListeners.get(this).textChanged(editable);
            }
        };
        for(TextView view: views) {
            view.addTextChangedListener(textWatcher);
        }
        textChangeListeners.put(textWatcher, changeCallback);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if(getFragmentLayout() != 0) return inflater.inflate(getFragmentLayout(), container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected int getFragmentLayout() {
        return 0;
    }

    protected void setOnDoneListener(TextView textView, final OnDoneListener onDoneListener) {
        textView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    onDoneListener.onDone();
                }
                return false;
            }
        });
    }

    protected void showSelect(Context context,
                              @ArrayRes int res,
                              final SelectListener listener) {
        showSelect(context, res, listener, false);
    }

    protected void showSelect(Context context,
                              @ArrayRes int res,
                              final SelectListener listener, boolean notCancelable) {
        final String[] titles = context.getResources().getStringArray(res);
        showSelect(context, titles, listener, notCancelable);
    }

    protected void showSelect(Context context,
                              final String[] items,
                              final SelectListener listener, boolean notCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onSelect(items[i], i);
            }
        });
        builder.setCancelable(!notCancelable);
        builder.show();
    }

    @CrashSafe
    public void hideKeyboard() {
        try {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ignored) {

        }
    }

    public void showKeyboard(EditText view) {
        view.requestFocus();
        InputMethodManager keyboard = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(view, 0);
    }

    public void showMessage(String message) {
        UITools.toastLong(getActivity(), message);
    }

    public void showMessage(CharSequence message, int code) {
        UITools.toastLong(getActivity(), message);
    }
}
