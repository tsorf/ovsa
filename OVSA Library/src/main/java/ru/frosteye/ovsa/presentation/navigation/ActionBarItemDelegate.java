package ru.frosteye.ovsa.presentation.navigation;

/**
 * Created by ovcst on 03.08.2017.
 */

public interface ActionBarItemDelegate {
    void onBarAction(int id);
}
