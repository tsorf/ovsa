package ru.frosteye.ovsa.presentation.navigation.impl;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import ru.frosteye.ovsa.presentation.navigation.FragmentNavigator;

/**
 * Created by oleg on 28.06.16.
 */

@Deprecated
public class FragmentNavigatorImpl<T extends Fragment> implements FragmentNavigator<T> {
    private int containerResource = 0;

    @Inject
    public FragmentNavigatorImpl() {
    }

    @Override
    public void setContainerResource(@IdRes int resource) {
        containerResource = resource;
    }

    @Override
    public void showFragment(AppCompatActivity activity, T fragment) {
        showFragment(activity, fragment, false);
    }

    @Override
    public void showFragment(AppCompatActivity activity, T fragment, boolean backstack) {
        if(containerResource == 0) throw new RuntimeException("Fragment container is not defined");
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        if(backstack) transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.replace(containerResource, fragment);
        transaction.commit();
    }
}
