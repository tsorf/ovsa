package ru.frosteye.ovsa.presentation.adapter;

import android.view.View;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by oleg on 12.07.17.
 */

public class ModelViewHolder<V extends View> extends FlexibleViewHolder {

    public final V view;

    public ModelViewHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);
        this.view = ((V) view);
    }
}
