package ru.frosteye.ovsa.presentation.effect;

import android.support.design.widget.AppBarLayout;

/**
 * Created by oleg on 30.07.17.
 */

public abstract class AppBarStateListener implements AppBarLayout.OnOffsetChangedListener {

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private State currentState = State.IDLE;

    @Override
    public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (i == 0) {
            if (currentState != State.EXPANDED) {
                onStateChanged(appBarLayout, State.EXPANDED);
            }
            currentState = State.EXPANDED;
        } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
            if (currentState != State.COLLAPSED) {
                onStateChanged(appBarLayout, State.COLLAPSED);
            }
            currentState = State.COLLAPSED;
        } else {
            if (currentState != State.IDLE) {
                onStateChanged(appBarLayout, State.IDLE);
            }
            currentState = State.IDLE;
        }
        int verticalOffset = Math.abs(i);
        float halfScrollRange = (int) (appBarLayout.getTotalScrollRange() * 0.5f);
        float ratio = (float) verticalOffset / halfScrollRange;
        ratio = Math.max(0f, Math.min(1f, ratio));
    }

    public abstract void onRatioChanged(float ratio);

    public abstract void onStateChanged(AppBarLayout appBarLayout, State state);
}
