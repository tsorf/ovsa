package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import ru.frosteye.ovsa.R;

/**
 * Created by oleg on 05.12.16.
 */

public class Line extends View {
    public Line(Context context) {
        super(context);
        init(context.obtainStyledAttributes(R.styleable.Line));

    }

    public Line(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.Line));

    }

    public Line(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.Line, defStyleAttr, 0));

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Line(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context.obtainStyledAttributes(attrs, R.styleable.Line, defStyleAttr, defStyleRes));
    }

    private void init(TypedArray array) {

    }
}
