package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import ru.frosteye.ovsa.R;


/**
 * Created by oleg on 02.12.16.
 */

public class StaticProgressBarView extends View {
    public static final int PADDING = 0;

    private int progress = 0;
    private Paint borderPaint;
    private Paint filledPaint;
    private Path borderPath, fillPath, maskPath;
    private Paint maskPaint;
    private RectF arc;
    private int fillColor, borderColor, maskColor;
    private float cornerRadius = 5;
    private float borderWidth = 3;


    public StaticProgressBarView(Context context) {
        super(context);
        init(context.obtainStyledAttributes(R.styleable.StaticProgressBarView));
    }

    public StaticProgressBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.StaticProgressBarView));
    }

    public StaticProgressBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.StaticProgressBarView, defStyleAttr, 0));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StaticProgressBarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context.obtainStyledAttributes(attrs, R.styleable.StaticProgressBarView, defStyleAttr, defStyleRes));
    }

    private void init(TypedArray array) {
        if(array.hasValue(R.styleable.StaticProgressBarView_initialProgress)) {
            progress = array.getInt(R.styleable.StaticProgressBarView_initialProgress, 0);
        }
        if(array.hasValue(R.styleable.StaticProgressBarView_barFillColor)) {
            fillColor = array.getColor(R.styleable.StaticProgressBarView_barFillColor, 0);
        }
        if(array.hasValue(R.styleable.StaticProgressBarView_maskColor)) {
            maskColor = array.getColor(R.styleable.StaticProgressBarView_maskColor, 0);
        }
        if(array.hasValue(R.styleable.StaticProgressBarView_barBorderColor)) {
            borderColor = array.getColor(R.styleable.StaticProgressBarView_barBorderColor, 0);
        }
        if(array.hasValue(R.styleable.StaticProgressBarView_barBorderWidth)) {
            borderWidth = array.getDimension(R.styleable.StaticProgressBarView_barBorderWidth, 0);
        }
        if(array.hasValue(R.styleable.StaticProgressBarView_barCornerRadius)) {
            cornerRadius = array.getDimension(R.styleable.StaticProgressBarView_barCornerRadius, 5);
        }

        filledPaint = new Paint();
        if(fillColor != 0) {
            filledPaint.setColor(fillColor);
        } else {
            filledPaint.setColor(getContext().getResources().getColor(R.color.ovsaDefaultAccentColor));
        }
        filledPaint.setAntiAlias(true);
        filledPaint.setStyle(Paint.Style.FILL);

        maskPaint = new Paint();
        if(maskColor != 0) {
            maskPaint.setColor(maskColor);
        } else {
            maskPaint.setColor(Color.WHITE);
        }
        maskPaint.setAntiAlias(true);
        maskPaint.setStyle(Paint.Style.FILL);

        borderPaint = new Paint();
        if(borderColor != 0) {
            borderPaint.setColor(borderColor);
        } else {
            borderPaint.setColor(Color.LTGRAY);
        }
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(borderWidth);

        borderPath = new Path();
        fillPath = new Path();
        maskPath = new Path();
        arc = new RectF();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawProgress(canvas);
    }

    public void setProgress(int progress) {
        this.progress = progress;
        if(this.progress > 100) this.progress = 100;
        if(this.progress < 0) this.progress = 0;
        invalidate();
    }

    private void drawProgress(Canvas canvas) {
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        float leftTopX = PADDING + borderWidth / 2, leftTopY = height - height / 3;
        float leftBottomX = PADDING + borderWidth / 2, leftBottomY = height - PADDING - borderWidth / 2;
//        if((height - leftTopY) / 2 < cornerRadius)
//            cornerRadius = (height - leftTopY) / 2;
        float rightTopX = width - PADDING - borderWidth / 2, rightTopY = PADDING;
        float rightBottomX = width - PADDING - borderWidth / 2, rightBottomY = height - PADDING - borderWidth / 2;

        float progressXEdge = (width) / 100 * progress;

        if(progress != 0) {
            fillPath.reset();
            fillPath.moveTo(PADDING, PADDING);
            fillPath.lineTo(PADDING, height - PADDING);
            fillPath.lineTo(progressXEdge, height - PADDING);
            fillPath.lineTo(progressXEdge, PADDING);
            fillPath.close();
            canvas.drawPath(fillPath, filledPaint);
        }

        maskPath.reset();
        maskPath.moveTo(PADDING, PADDING);
        maskPath.lineTo(PADDING, leftTopY);
        maskPath.lineTo(rightTopX, PADDING);
        maskPath.close();
        canvas.drawPath(maskPath, maskPaint);

        borderPath.reset();
        borderPath.moveTo(leftTopX, leftTopY + cornerRadius);
        borderPath.lineTo(leftBottomX, leftBottomY - cornerRadius);
        borderPath.lineTo(rightBottomX - cornerRadius, rightBottomY);

//        arc.set(leftBottomX, leftBottomY - cornerRadius, leftBottomX + cornerRadius, leftBottomY);
//        borderPath.arcTo(arc, 180, 270);

        borderPath.lineTo(rightTopX, rightTopY + cornerRadius);

        borderPath.lineTo(rightTopX, rightTopY + cornerRadius);

        borderPath.lineTo(leftTopX + cornerRadius, leftTopY);

        borderPath.close();
        canvas.drawPath(borderPath, borderPaint);
    }
}
