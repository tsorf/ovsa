package ru.frosteye.ovsa.presentation.view.widget;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

/**
 * Created by ovcst on 16.05.2017.
 */

public class BlinkingTextView extends AppCompatTextView {

    private Animation anim;

    public BlinkingTextView(Context context) {
        super(context);
    }

    public BlinkingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlinkingTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        anim = new AlphaAnimation(0.4f, 1.0f);
        anim.setDuration(1000);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        startAnimation(anim);
    }

    @Override
    public void setVisibility(int visibility) {
        if(visibility == GONE) {
            clearAnimation();
        } else {
            startAnimation(anim);
        }
        super.setVisibility(visibility);
    }
}
