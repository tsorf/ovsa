package ru.frosteye.ovsa.presentation.navigation;

import android.content.Context;

import ru.frosteye.ovsa.presentation.view.BasicView;

/**
 * Created by oleg on 10.07.17.
 */

public interface NavigatorView extends BasicView {
    Context getContext();
}
