package ru.frosteye.ovsa.presentation.view;

/**
 * Created by oleg on 27.06.16.
 */
public interface BaseView {
    void showMessage(String message);
    void enableControls(boolean enabled);
}
