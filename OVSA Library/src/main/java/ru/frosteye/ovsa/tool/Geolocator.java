package ru.frosteye.ovsa.tool;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by user on 15.06.16.
 */

@Singleton
public class Geolocator {
    public interface GeocoderCallback {
        void onResult(Location location);
    }

    public interface ReverseGeocoderCallback {
        void onResult(String string);
    }

    public enum TrackingMode {SOFT, NORMAL, HARD}
    public static final int QUEUE_SIZE_LIMIT = 15;
    public static final int SOFT_INTERVAL = 60 * 1000 * 3;
    public static final int NORMAL_INTERVAL = 60 * 1000;
    public static final int HARD_INTERVAL = 5 * 1000;


    private Context context;
    private LocationManager locationManager;
    private PriorityQueue<Location> fineLocations;
    private Location checkPoint;

    @Inject
    public Geolocator(Context context) {
        this.context = context;
        this.locationManager = ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE));
        this.checkPoint = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    private Comparator<Location> accuracySorter = new Comparator<Location>() {
        @Override
        public int compare(Location lhs, Location rhs) {
            if(lhs.getAccuracy() > rhs.getAccuracy()) {
                return 1;
            } else return -1;
        }
    };

    public void startTracking(TrackingMode mode) {
        fineLocations = new PriorityQueue<>(QUEUE_SIZE_LIMIT, accuracySorter);
        int interval = 0;
        switch (mode) {
            case SOFT:
                interval = SOFT_INTERVAL;
                break;
            case NORMAL:
                interval = NORMAL_INTERVAL;
                break;
            case HARD:
                interval = HARD_INTERVAL;
                break;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, interval, 0, locationListener);
    }

    public void stopTracking() {
        locationManager.removeUpdates(locationListener);
        Location location = fineLocations.peek();
        if(location != null) {
            checkPoint = location;
        }
        fineLocations = null;
    }

    private void processLocation(Location location) {
        if(fineLocations.size() == QUEUE_SIZE_LIMIT) {
            checkPoint = fineLocations.peek();
            fineLocations = new PriorityQueue<>();
        }
        fineLocations.offer(location);
    }

    public Location getLocation() {
        Location location = fineLocations.peek();
        if(location == null) {
            location = checkPoint;
        }
        return location;
    }

    public Location getLastLocation() {
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            return location;
        } else {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(location != null) {
                return location;
            }
        }
        return null;
    }

    public Location geocode(String addr) {
        Geocoder geocoder = new Geocoder(context);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(addr, 1);
            if(addresses.size() > 0) {
                Location location = new Location(LocationManager.GPS_PROVIDER);
                location.setLatitude(addresses.get(0).getLatitude());
                location.setLongitude(addresses.get(0).getLongitude());
                return location;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String geocode(Location location) {
        Geocoder geocoder = new Geocoder(context);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if(addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder builder = new StringBuilder();
                for(int i = address.getMaxAddressLineIndex(); i >= 0 ; i--) {
                    builder.append(", ");
                    builder.append(address.getAddressLine(i));
                }
                return builder.toString().substring(2);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void geocodeAsync(final String location, final GeocoderCallback callback) {
        new AsyncTask<Void, Void, Location>() {

            @Override
            protected Location doInBackground(Void... params) {
                return geocode(location);
            }

            @Override
            protected void onPostExecute(Location location) {
                callback.onResult(location);
            }
        }.execute();
    }

    public void geocodeAsync(final Location location, final ReverseGeocoderCallback callback) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                return geocode(location);
            }

            @Override
            protected void onPostExecute(String location) {
                callback.onResult(location);
            }
        }.execute();
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            processLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
}
