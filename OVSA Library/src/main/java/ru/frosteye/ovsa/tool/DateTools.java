package ru.frosteye.ovsa.tool;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.frosteye.ovsa.R;

/**
 * Created by oleg on 01.07.16.
 */
public class DateTools {

    public interface DatePickerCallback {
        void onDateSelected(Date date);
    }
    public interface CalendarDatePickerCallback extends DatePickerCallback {
        void onCalendarReady(Calendar calendar);
    }
    public interface TimePickerCallback {
        void onTimeSelected(String time);
        void onRaw(int hour, int minute);
    }

    private static SimpleDateFormat dottedDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private static SimpleDateFormat dottedDateFormatWithTimeWithoutSeconds = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
    private static SimpleDateFormat dashedDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat dottedDateFormatWithTime = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private static SimpleDateFormat dayOfMonthFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
    private static SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());

    public static String formatDottedDateWithTime(Date date) {
        if(date == null) return null;
        return dottedDateFormatWithTime.format(date);
    }

    public static String formatDottedDateWithTimeWithoutSeconds(Date date) {
        if(date == null) return null;
        return dottedDateFormatWithTimeWithoutSeconds.format(date);
    }

    public static String formatTime(Date date) {
        if(date == null) return null;
        return timeFormat.format(date);
    }

    public static String formatDashedDate(Date date) {
        if(date == null) return null;
        return dashedDateFormat.format(date);
    }

    public static String formatPrettyDate(Date date) {
        if(date == null) return null;
        return prettyDateFormat.format(date);
    }

    public static String formatDottedDate(Date date) {
        if(date == null) return null;
        return dottedDateFormat.format(date);
    }

    public static Date parseDottedDate(String date) {
        if(date == null) return null;
        try {
            return dottedDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date parseDottedDateWithTimeWithoutSeconds(String date) {
        if(date == null) return null;
        try {
            return dottedDateFormatWithTimeWithoutSeconds.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDayOfMonth(Date date) {
        if(date == null) return null;
        return dayOfMonthFormat.format(date);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback) {
        showDateDialog(context, callback, null);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, Calendar initial) {
        showDateDialog(context, callback, initial, false);
    }

    public static void showDateDialog(Context context, final DatePickerCallback callback, final Calendar initial, boolean toFuture) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        if(toFuture) {
            datePicker.setMinDate(new Date().getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                initial.set(year, month, day);
                callback.onDateSelected(initial.getTime());
                if(callback instanceof CalendarDatePickerCallback) {
                    ((CalendarDatePickerCallback) callback).onCalendarReady(initial);
                }
            }
        });
        d.show();
    }

    public static void showDateDialogWithButtons(Context context, final DatePickerCallback callback, Calendar initial) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static void showDateDialogWithButtons(Context context, final DatePickerCallback callback, Calendar initial, boolean fromToday) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.date_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if(initial != null) {
            datePicker.init(
                    initial.get(Calendar.YEAR), initial.get(Calendar.MONTH), initial.get(Calendar.DAY_OF_MONTH), null
            );
        }
        if(fromToday) {
            datePicker.setMinDate(new Date().getTime());
        }
        datePicker.setCalendarViewShown(false);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                callback.onDateSelected(calendar.getTime());
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static String formatTime(int hour, int minute) {
        String h = hour < 10 ? "0" + hour : String.valueOf(hour);
        String m = minute < 10 ? "0" + minute : String.valueOf(minute);
        return String.format("%s:%s", h, m);
    }

    public static void showTimeDialogWithButtons(Context context, final TimePickerCallback callback) {
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.time_picker, null);
        view.findViewById(R.id.datePickerButtons).setVisibility(View.VISIBLE);
        final Button cancel = (Button) view.findViewById(R.id.datePickerCancel);
        final Button ok = (Button) view.findViewById(R.id.datePickerOk);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        d.setContentView(view);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hour, min;
                if (Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = timePicker.getHour();
                    min = timePicker.getMinute();
                } else {
                    hour = timePicker.getCurrentHour();
                    min = timePicker.getCurrentMinute();
                }
                callback.onRaw(hour, min);
                callback.onTimeSelected(formatTime(hour, min));
                d.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.cancel();
            }
        });
        d.show();
    }

    public static void showTimeDialog(Context context, final TimePickerCallback callback) {
        Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(R.layout.time_picker, null);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        d.setContentView(view);
        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                int hour, min;
                if (Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = timePicker.getHour();
                    min = timePicker.getMinute();
                } else {
                    hour = timePicker.getCurrentHour();
                    min = timePicker.getCurrentMinute();
                }
                callback.onRaw(hour, min);
                callback.onTimeSelected(formatTime(hour, min));
            }
        });
        d.show();
    }

    public static String formatMillisecondsToTime(int time) {
        int minutes = time / (60 * 1000);
        int seconds = (time / 1000) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }

    public static String formatSecondsToTime(int time) {
        int minutes = time / (60);
        int seconds = (time) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }
}
