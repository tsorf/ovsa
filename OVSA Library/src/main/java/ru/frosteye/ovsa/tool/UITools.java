package ru.frosteye.ovsa.tool;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.hardware.Camera;
import android.support.annotation.ArrayRes;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

import ru.frosteye.ovsa.R;
import ru.frosteye.ovsa.stub.listener.SelectListener;

/**
 * Created by oleg on 15.06.16.
 */
public class UITools {
    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void toastLong(Context context, CharSequence message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
    public static void toastLong(Context context, int message) {
        toastLong(context, context.getString(message));
    }
    public static void toastShort(Context context, CharSequence message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void toastShort(Context context, int message) {
        toastShort(context, context.getString(message));
    }

    public static AlertDialog simpleAlert(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ovsa_string_ok, listener).show();
    }

    public static AlertDialog simpleAlert(Context context, int title, int message) {
        return simpleAlert(context, context.getString(title), context.getString(message), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
    }

    public static AlertDialog simpleAlert(Context context, int title, int message, DialogInterface.OnClickListener listener) {
        return simpleAlert(context, context.getString(title), context.getString(message), listener);
    }

    public static AlertDialog simpleAlert(Context context, int title, String message) {
        return simpleAlert(context, context.getString(title), message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
    }

    public static AlertDialog simpleAlert(Context context, String title, int message) {
        return simpleAlert(context, title, context.getString(message), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
    }

    public static void applyDialogLayoutDimensions(Dialog dialog, int width, int height) {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = width;
        lp.height = height;
        window.setAttributes(lp);
    }

    public static AlertDialog simpleAlert(Context context, String title, String message) {
        return simpleAlert(context, title, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
    }

    public static void selector(Context context, @ArrayRes int array, SelectListener selectListener) {
        selector(context, context.getResources().getStringArray(array), selectListener);
    }

    public static void selector(Context context, final String[] array, final SelectListener selectListener) {
        new AlertDialog.Builder(context)
                .setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectListener.onSelect(array[i], i);
                    }
                })
                .show();
    }

    public static void selector(Context context, String title, final String[] array, final SelectListener selectListener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectListener.onSelect(array[i], i);
                    }
                })
                .show();
    }


    public static void selectorNonCancelable(Context context, String title, final String[] array, final SelectListener selectListener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setCancelable(false)
                .setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectListener.onSelect(array[i], i);
                    }
                })
                .show();
    }

    public static void confirm(Context context, int title, SimpleConfirmCallback simpleConfirmCallback) {
        confirm(context, context.getString(title), null, simpleConfirmCallback);
    }

    public static void confirm(Context context, int title, int message, SimpleConfirmCallback simpleConfirmCallback) {
        confirm(context, context.getString(title), context.getString(message), simpleConfirmCallback);
    }

    public static void confirm(Context context, String title, String message, final SimpleConfirmCallback simpleConfirmCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        if(message != null) builder.setMessage(message);
        builder.setPositiveButton(R.string.ovsa_string_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                simpleConfirmCallback.yes();
            }
        });
        if(simpleConfirmCallback instanceof ConfirmCallback) {
            builder.setNegativeButton(R.string.ovsa_string_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ((ConfirmCallback)simpleConfirmCallback).no();
                }
            });
        }
        builder.show();
    }

    public static void setCameraDisplayOrientation(Activity activity, android.hardware.Camera camera) {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        if(cameraId == -1) return;
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    public static File saveBitmapToJpgFile(File directory, String name, Bitmap bitmap) {
        try {
            if(!directory.exists())
                directory.mkdirs();
            File file = new File(directory, name + ".jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File saveBitmapToFile(File directory, String name, Bitmap bitmap, String ext) {
        try {
            if(!directory.exists())
                directory.mkdirs();
            File file = new File(directory, name + "." + ext);
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Point getWindowDimens(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public interface SimpleConfirmCallback {
        void yes();
    }

    public interface ConfirmCallback extends SimpleConfirmCallback {
        void no();
    }
}
