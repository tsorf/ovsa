package ru.frosteye.ovsa.tool;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.frosteye.ovsa.marker.UniqueItem;

/**
 * Created by oleg on 15.06.16.
 */
public class TextTools {
    /**
     * Cuts string source to conform defined length.
     * @param source {@link String} source.
     * @param length length for resulting string.
     * @return the resulting string.
     */
    public static String cut(String source, int length) {
        if(source.length() <= length) return source;
        return String.format("%s...", source.substring(0, length - 3));
    }
    /**
     * Checks if {@link TextView}'s content is empty.
     * @param textView {@link TextView} to check.
     * @return check result
     */
    public static boolean isEmpty(TextView textView) {
        return textView.getText().toString().isEmpty();
    }

    /**
     * Checks if {@link TextView}'s content is empty (applying {@link String#trim()}).
     * @param textView {@link TextView} to check.
     * @return result.
     */
    public static boolean isTrimmedEmpty(TextView textView) {
        return textView.getText().toString().trim().isEmpty();
    }

    public static String extract(TextView textView) {
        return textView.getText().toString();
    }

    public static String extractTrimmed(TextView textView) {
        return textView.getText().toString().trim();
    }

    public static CharSequence trimParagraph(CharSequence text) {
        if(text.length() == 0) return text;
        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }

    public static String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static boolean validatePhone(String string) {
        return PhoneNumberUtils.isGlobalPhoneNumber(string) && string.length() == 12;
    }

    public static boolean validateEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean validateEmail(EditText input) {
        return validateEmail(TextTools.extractTrimmed(input));
    }

    public static String concatIds(Collection<UniqueItem> collection) {
        if(collection.isEmpty()) return null;
        Iterator<UniqueItem> iterator = collection.iterator();
        StringBuilder builder = new StringBuilder();
        while (iterator.hasNext()) {
            builder.append(",").append(iterator.next().getId());
        }
        return builder.toString().substring(1);
    }

    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {

        final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Map<String, Typeface> newMap = new HashMap<String, Typeface>();
            newMap.put("serif", customFontTypeface);
            try {
                final Field staticField = Typeface.class
                        .getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                staticField.set(null, newMap);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
                defaultFontTypefaceField.setAccessible(true);
                defaultFontTypefaceField.set(null, customFontTypeface);
            } catch (Exception e) {
                Log.e(TextTools.class.getSimpleName(), "Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
            }
        }
    }

    public static String loadStringFromRawResource(Resources resources, int resId) {
        InputStream rawResource = resources.openRawResource(resId);
        String content = streamToString(rawResource);
        try {rawResource.close();} catch (IOException e) {}
        return content;
    }
    private static String streamToString(InputStream in) {
        String l;
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder s = new StringBuilder();
        try {
            while ((l = r.readLine()) != null) {
                s.append(l + "\n");
            }
        } catch (IOException e) {}
        return s.toString();
    }

    public static String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }
}
