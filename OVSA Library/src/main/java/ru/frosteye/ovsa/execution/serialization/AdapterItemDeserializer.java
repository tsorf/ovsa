package ru.frosteye.ovsa.execution.serialization;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;

/**
 * Created by oleg on 12.07.17.
 */

public abstract class AdapterItemDeserializer<T, Wrapper extends AdapterItem<T, ?>>
        implements JsonDeserializer<Wrapper> {

    @Override
    public final Wrapper deserialize(JsonElement json, Type typeOfT,
                         JsonDeserializationContext context) throws JsonParseException {

        try {
            T object = context.deserialize(json, provideType());
            Wrapper wrapper = provideWrapperType().newInstance();
            wrapper.setModel(object);
            return wrapper;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    protected abstract Class<T> provideType();
    protected abstract Class<Wrapper> provideWrapperType();
}
