package ru.frosteye.ovsa.execution.network.request;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by oleg on 19.12.16.
 */

public class RequestParams extends HashMap<String, String> implements Serializable {

    public RequestParams() {
        super();
    }

    public RequestParams addParam(String key, Object value) {
        put(key, String.valueOf(value));
        return this;
    }
}
