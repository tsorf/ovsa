package ru.frosteye.ovsa.execution.task;

import ru.frosteye.ovsa.presentation.view.BaseView;


@Deprecated
public class ResultSubscriber<V extends BaseView, T> extends SimpleSubscriber<T> {

    public interface OnNext<T> {
        void onNext(T result);
    }

    private V view;
    private OnNext<T> onNext;

    public ResultSubscriber(V view) {
        this.view = view;
    }

    public ResultSubscriber(V view, OnNext<T> onNext) {
        this.view = view;
        this.onNext = onNext;
    }

    @Override
    public void onNext(T t) {
        view.enableControls(true);
        if(onNext != null)
            onNext.onNext(t);
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        view.enableControls(true);
        view.showMessage(e.getMessage());
    }
}
