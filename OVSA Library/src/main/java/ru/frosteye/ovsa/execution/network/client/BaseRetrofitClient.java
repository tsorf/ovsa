package ru.frosteye.ovsa.execution.network.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.frosteye.ovsa.data.constant.OvsaKeys;

/**
 * Created by oleg on 20.12.16.
 */

public abstract class BaseRetrofitClient<T> {

    private T api;
    private IdentityProvider identityProvider;

    @Deprecated
    private String token;

    public BaseRetrofitClient(String baseUrl) {
        init(baseUrl, null);
    }

    public BaseRetrofitClient(String baseUrl, IdentityProvider identityProvider) {
        init(baseUrl, identityProvider);
    }

    private void init(String baseUrl, IdentityProvider identityProvider) {
        this.identityProvider = identityProvider;
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        List<Interceptor> interceptors = createClientInterceptors();
        if(interceptors != null) {
            for(Interceptor interceptor: interceptors) {
                clientBuilder.addInterceptor(interceptor);
            }
        }
        clientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        clientBuilder.readTimeout(30, TimeUnit.SECONDS);

        Gson gson = createGson();
        if(gson == null) gson = new Gson();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        this.api = retrofit.create(apiClass());
    }

    @Deprecated
    public void setToken(String token) {
        this.token = token;
    }

    public T getApi() {
        return api;
    }

    public abstract Class<T> apiClass();

    protected Gson createGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

    protected List<Interceptor> createClientInterceptors() {
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                if(getToken() != null) {
                    builder.addHeader(getAuthHeaderName(), getToken());
                }
                for(Map.Entry<String, String> entry: getHeaders().entrySet()) {
                    builder.addHeader(entry.getKey(), entry.getValue());
                }
                return chain.proceed(builder.build());
            }
        });
        interceptors.add(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return interceptors;
    }

    protected String getAuthHeaderName() {
        return OvsaKeys.AUTHORIZATION;
    }

    protected Map<String, String> getHeaders() {
        return new HashMap<>();
    }

    protected String getToken() {
        if(identityProvider != null) {
            return identityProvider.provideIdentity();
        }
        return token;
    }
}
