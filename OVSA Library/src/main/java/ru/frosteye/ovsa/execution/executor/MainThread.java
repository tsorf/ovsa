package ru.frosteye.ovsa.execution.executor;

import rx.Scheduler;

/**
 * Created by oleg on 14.06.16.
 */

/**
 * Representing Android's UI thread
 */
public interface MainThread {
    Scheduler getScheduler();
}
