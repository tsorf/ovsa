package ru.frosteye.ovsa.execution.task;

import rx.Subscriber;

/**
 * Created by user on 16.06.16.
 */
public class SimpleSubscriber<T> extends Subscriber<T> {

    public static final int ERROR = 0;
    public static final int SUCCESS = 1;

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {

    }
}
