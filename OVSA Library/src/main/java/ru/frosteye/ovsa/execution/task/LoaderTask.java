package ru.frosteye.ovsa.execution.task;

import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

import retrofit2.Call;
import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.base.NetworkTask;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by ovcst on 06.04.2017.
 */

public abstract class LoaderTask<P, R> extends NetworkTask<P, R> {

    
    public LoaderTask(MainThread mainThread,
                      Executor executor ) {
        super(mainThread, executor);
    }

    @Override
    protected Observable<R> prepareObservable(final P p) {
        return Observable.create(new Observable.OnSubscribe<R>() {
            @Override
            public void call(Subscriber<? super R> subscriber) {
                try {
                    if(cachedFirst()) processCached(subscriber);
                    R result = executeCall(getCall(p));
                    getRepo().save(result);
                    subscriber.onNext(result);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    e.printStackTrace();
                    if(!cachedFirst()) processCached(subscriber);
                    subscriber.onError(e);
                }
            }
        });
    }

    private void processCached(Subscriber<? super R> subscriber) {
        R cachedResult = getRepo().load();
        if(cachedResult != null)
            subscriber.onNext(cachedResult);
    }

    protected boolean cachedFirst() {
        return false;
    }

    @NonNull
    protected abstract Repo<R> getRepo();

    @NonNull
    protected abstract Call<R> getCall(P params);
}
