package ru.frosteye.ovsa.execution.task;

import java.util.concurrent.Executor;

import ru.frosteye.ovsa.execution.executor.MainThread;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by oleg on 14.06.16.
 */

/**
 * The base worker class, that represents any Observable-based operations
 * @param <Params> input param's type
 * @param <Result> the result of this task
 */
public abstract class ObservableTask<Params, Result> {
    private MainThread mainThread;
    private Executor executor;
    private Subscription subscription = Subscriptions.empty();

    public ObservableTask(MainThread mainThread, Executor executor) {
        this.mainThread = mainThread;
        this.executor = executor;
    }

    protected abstract Observable<Result> prepareObservable(Params params);

    public void cancel() {
        if(subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    public void execute(Params params, Subscriber<Result> subscriber) {
        subscription = createObservable(params).subscribe(subscriber);
    }

    private Observable<Result> createObservable(Params params){
        cancel();
        return prepareObservable(params)
                .subscribeOn(getExecutionScheduler())
                .materialize()
                .observeOn(getObserveScheduler())
                .dematerialize();
    }

    public void execute(Params params) {
        subscription = createObservable(params).subscribe();
    }

    protected Scheduler getExecutionScheduler() {
        return Schedulers.from(executor);
    }

    protected Scheduler getObserveScheduler() {
        return mainThread.getScheduler();
    }
}
