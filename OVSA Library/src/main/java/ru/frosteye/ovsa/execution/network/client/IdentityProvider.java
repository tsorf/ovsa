package ru.frosteye.ovsa.execution.network.client;

/**
 * Created by oleg on 21.06.17.
 */

public interface IdentityProvider {
    String provideIdentity();
}
