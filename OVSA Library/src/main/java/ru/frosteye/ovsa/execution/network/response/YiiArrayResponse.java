package ru.frosteye.ovsa.execution.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.frosteye.ovsa.data.constant.OvsaKeys;

/**
 * Created by oleg on 20.12.16.
 */

public class YiiArrayResponse<T> {

    @SerializedName(OvsaKeys.META)
    private Meta meta;

    public Meta getMeta() {
        return meta;
    }

    private List<T> items;

    public List<T> getItems() {
        return items;
    }

    @SerializedName(OvsaKeys.LINKS)
    private Links links;

    public Links getLinks() {
        return links;
    }

    public static class Meta {
        private int totalCount;
        private int pageCount;
        private int currentPage;
        private int perPage;

        public int getTotalCount() {
            return totalCount;
        }

        public int getPageCount() {
            return pageCount;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public int getPerPage() {
            return perPage;
        }
    }

    public static class Link {
        private String href;

        public String getUrl() {
            return href;
        }
    }

    public static class Links {
        private Link self;

        public Link getSelf() {
            return self;
        }
    }
}
