package ru.frosteye.ovsa.execution.network.response;

/**
 * Created by ovcst on 07.01.2017.
 */

public class MessageResponse {
    private String message;
    private int code;

    public MessageResponse(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
