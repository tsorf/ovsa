package ru.frosteye.ovsa.execution.network.base;

/**
 * Created by oleg on 21.12.16.
 */

public class ApiException extends RuntimeException {

    private int code = 0;

    public ApiException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public ApiException(Throwable cause) {
        super(cause);
    }
}
