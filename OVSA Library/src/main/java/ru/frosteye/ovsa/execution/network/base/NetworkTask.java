package ru.frosteye.ovsa.execution.network.base;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Response;
import ru.frosteye.ovsa.R;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.base.ApiException;
import ru.frosteye.ovsa.execution.network.response.MessageResponse;
import ru.frosteye.ovsa.execution.task.ObservableTask;
import rx.Observable;
import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

/**
 * Created by ovcst on 07.01.2017.
 */

public abstract class NetworkTask<P, Res> extends ObservableTask<P, Res> {

    protected static final Gson GSON = new Gson();

    public NetworkTask(MainThread mainThread, Executor executor) {
        super(mainThread, executor);
    }

    protected <Result> Result executeCall(Call<Result> call) throws Exception {
        try {
            Response<Result> response = call.execute();
            if(response.isSuccessful()) return response.body();
            MessageResponse messageResponse = GSON.fromJson(response.errorBody().string(), MessageResponse.class);
            throw new ApiException(messageResponse.getMessage(), messageResponse.getCode());
        } catch (IOException e) {
            e.printStackTrace();
            throw new ApiException(getString(R.string.default_connection_error), 503);
        }

    }
}
